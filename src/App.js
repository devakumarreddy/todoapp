import React, { Component } from 'react';
import './App.css';
import TodoInput from './components/todoInput';
import TodoItem from './components/todoItem';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [ ],
      done:[],
      nextId: 0
    };

    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
  }

  addTodo(todoText) {
    let todos = this.state.todos.slice();
    
    
    todos.push({id: this.state.nextId, text: todoText});
  
    this.setState({
      todos: todos,
      nextId: ++this.state.nextId
    });}
  

  removeTodo(id) {
    this.setState({
        done:  this.state.done.filter((todo, index) => todo.id === id),
        todos: this.state.todos.filter((todo, index) => todo.id !== id)
      });
  }
  adddone(id){

  }

  render() {
    return (
      <div className="App">
        <div className="todo-wrapper">
      
          <TodoInput addTodo={this.addTodo} />
          
            {
              this.state.todos.map((todo) => {
                return <TodoItem todo={todo} key={todo.id} id={todo.id} removeTodo={this.removeTodo}/>
              })
            }
          
        </div>
      </div>
    );
  }
}

export default App;
/*import React, { Component } from 'react';
import './App.css';
import TodoInput from './components/todoInput';
import TodoItem from './components/todoItem';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      nextId: 0
      
    };
    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    
  }
  

  addTodo(todoText) {
    let todos = this.state.todos.slice();
    console.log(this.state.todos);
    todos.push({id: this.state.nextId, text: todoText});
    this.setState({
      todos: todos,
      nextId: ++this.state.nextId
      
    });
  }
  
  removeTodo(id) {
    this.setState({
        todos: this.state.todos.filter((todo, index) => todo.id !== id)
             /* this.state.todos.filter((todo,index))=> todo.id==id)
      });
  }
  Adddone(id){
    this.setState({
       this.state.todos.filter((todo, index)) => todo.id==id)
    });
  }
  render() {
    return (
      <div className="App">
        
        <div className="todo-wrapper">
      
          <TodoInput  addTodo={this.addTodo} />
            {
                this.state.todos.map((todo) => {
                return <TodoItem todo={todo} key={todo.id} id={todo.id} removeTodo={this.removeTodo}/>
                /*return <TodoItem todo={todo} key={todo.id} id={todo.id} Adddone={this.Adddone}/>
              })
              
            }
        </div>
      </div>
    );
  }
}

export default App;*/